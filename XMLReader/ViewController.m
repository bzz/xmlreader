//
//  ViewController.m
//  XMLReader
//
//  Created by Mikhail Baynov on 30/05/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

#import "ViewController.h"
#import "XMLReader.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
   [super viewDidLoad];

   XMLReader *reader = [XMLReader shared];
   
   [reader addText:@"<some><challenge xmlns='urn:ietf:params:xml:ns:xmpp-sasl'>bm9uY2U9IjE2NDA5NTEyNjUiLHFvcD0iYXV0aCIsY2hhcnNldD11dGYtOCxhbGdvcml0aG09bWQ1LXNlc3M=</challenge>"];
   [reader addText:@"<challenge2 xmlns='urn:ietf:params:xml:ns:xmpp-sasl'>bm9uY2U9IjE2NDA5NTEyNjUiLHFvcD0iYXV0aCIsY2hhcnNldD11dGYtOCxhbGdvcml0aG09bWQ1LXNlc3M=</challenge2></some>"];
   
   
   NSLog(@"111 %@", reader.dictionaryFromText);
}


@end
