//
//  AppDelegate.h
//  XMLReader
//
//  Created by Mikhail Baynov on 30/05/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

